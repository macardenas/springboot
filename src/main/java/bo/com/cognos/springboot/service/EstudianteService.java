package bo.com.cognos.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import bo.com.cognos.springboot.dao.EstudianteRepository;
import bo.com.cognos.springboot.entidades.Estudiante;
import bo.com.cognos.springboot.entidades.exception.ApplicationException;


@Service
@Transactional(readOnly = true)
public class EstudianteService {
	
	@Autowired
    EstudianteRepository repository;
	
	public List<Estudiante> listar(){
		return repository.findAll();
	}

	public Estudiante obtenerPorCodigo(String codigo){
		return repository.findByCodigo(codigo);
	}
	
	public Estudiante obtener(Integer id){
		return repository.getOne(id);
	}

	
	@Transactional(rollbackFor = ApplicationException.class, noRollbackFor = RuntimeException.class,
			propagation = Propagation.REQUIRES_NEW)
	public void agregar(Estudiante estudiante) throws ApplicationException {		
		Estudiante existente = repository.findByCodigo(estudiante.getCodigo());
		if(existente != null) {
			throw new ApplicationException("Ya existe el estudiante con el codigo: " + estudiante.getCodigo());
		}
		repository.save(estudiante);
	}
	
	@Transactional(rollbackFor = ApplicationException.class)
	public void editar(Estudiante estudiante) throws ApplicationException {		
		Optional<Estudiante> existente = repository.findById(estudiante.getId());
		if(!existente.isPresent()) {
			throw new ApplicationException("No existe el estudiante");
		}
		repository.saveAndFlush(estudiante);
	}
	
	@Transactional(rollbackFor = ApplicationException.class, propagation = Propagation.REQUIRED)
	public void eliminar(Integer idEliminar) {
		repository.deleteById(idEliminar);
	}
	
	
}
